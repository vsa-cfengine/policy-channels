# Quick Tips for Diagnosing Policy Channels

Author: Mike Weilgart

Date: 24 March 2016

Updated: 24 March 2016

## What channel is each host on?

There is a whole script written for this.

```
cd .../policy_channels_framework/
./summarize_host_assignments.sh --help
# OR
.../policy_channels_framework/summarize_host_assignments.sh --help
```

You can look at a quick summary of how many hosts are assigned
to which channel by using the `-s` option.

You can look at a list of all hosts assigned to a particular
channel by just naming the channel on the command line.

You can view all hosts assigned to *all* channels by using
the `-a` option.

**Note:** This script reports on the actual channel assignment
files present on the hub in `/var/cfengine/host_channel_assignments`

### What channel is a SINGLE host on?

For just one host, look at the channel assignment file directly:

```
cd /var/cfengine/host_channel_assignments/
cat ${INSERT_HOSTNAME}.txt
# OR
cat /var/cfengine/host_channel_assignments/${INSERT_HOSTNAME}.txt
```

Note that channel assignment files don't contain trailing newlines.
For easier readability in the terminal, you may want to use:

```
cd /var/cfengine/host_channel_assignments/
awk 1 ${INSERT_HOSTNAME}.txt
# OR
awk 1 /var/cfengine/host_channel_assignments/${INSERT_HOSTNAME}.txt
```

## What policy version is loaded into a channel?

Look at the params file to see what policy version is being staged
to that policy channel.

```
cd /var/cfengine/channel_scripts/
grep ${INSERT_CHANNEL_NAME} params.sh
```

### Dynamic policy "versions"

Note that although we *typically* use a "git tag" in `params.sh`
(to declare a static version of our code not subject to change),
the `params.sh` file will allow a git branch name,
e.g. for an experimental policy channel where you want to test
the code on the master branch *before* tagging it.

In such a case you may want to know *precisely* which code version
a given host is using.

Run the following command on any given host to see exactly which
version of policy it is actually running:

```
cat /var/cfengine/inputs/policy_commit_id.dat
```

Take the commit hash given above, and *in your git repo*, run
any of the following commands to see where that commit hash fits
in your history.  (You probably won't need to run them all.)

```
git log -1 --decorate ${INSERT_COMMIT_HASH}
git branch -a --contains ${INSERT_COMMIT_HASH}
git log --graph --oneline --decorate ${INSERT_BRANCH_NAME}
```
