# Cleaning Up CFEngine Policy Channels

Author: Mike Weilgart  
Date: 14 December 2017

(Note: This was written as an email originally but stands as a
self-contained documented example of policy channel cleanup procedure.)

I've just done a channel cleanup of stale policy channels on X hub

1. I checked for directories existing in
  `/var/cfengine/policy_channels/` that were not being refreshed, indicating
  they were no longer listed in `params.sh`, by running the following (note
  the timestamps):

    ```
    # ls -ltr ../policy_channels/
    total 432
    ...
    #
    ```

    Note: A better command I later worked out to show this information is:

    ```
    find /var/cfengine/policy_channels/* -prune -mtime +1 -exec ls -ld {} +
    ```

2. I got the list of all 847 host assignment files assigning hosts to
  these non-updating channels using:

    ```
    cd ../channel_scripts/; ./summarize_host_assignments.sh $(cd ../policy_channels/; find * -prune -mtime +1) | sed '/^=/d' > list_of_orphaned_assignments.txt
    ```

3. I checked whether any of these hosts were still active on hub X
  (they weren't), as follows:

    ```
    # psql
    SET
    SET
    psql (9.3.15, server 9.3.11)
    Type "help" for help.

    cfdb=# create temp table orph (hostname text);
    CREATE TABLE
    cfdb=# \copy orph from list_of_orphaned_assignments.txt ;
    cfdb=# select * from orph limit 2;
               hostname
    -------------------------------
    host1.fqdn.net
    host2.fqdn.net
    (2 rows)

    cfdb=# select * from hosts natural join orph;
    hostname | hostkey | ipaddress | lastreporttimestamp | firstreporttimestamp
    ----------+---------+-----------+---------------------+----------------------
    (0 rows)

    cfdb=# \q
    #
    ```

4. I repeated the above check with the same list on the Superhub and
  found that 547 of these hosts are still in inventory, but are now on
  different hubs than Hub X, which is fine. 
5. The remaining 300 hostnames I put into a file called
  "gone_from_inventory.txt" on the Superhub for later handling (below).
6. As none of the 847 host assignment files to non-updating channels
  on Hub X referred to hosts actually connected to Hub X,
  I removed all those assignment files as follows:

    ```
    cd /var/cfengine
    cp -a host_channel_assignments host_channel_assignments.bak
    cd host_channel_assignments
    less ../channel_scripts/list_of_orphaned_assignments.txt   # To triple check contents
    \rm $(sed 's/$/.txt/' ../channel_scripts/list_of_orphaned_assignments.txt )
    ```

7. Next, I removed the stale channel directories themselves with:

    ```
    cd /var/cfengine/policy_channels
    find * -prune -mtime +1 -ls -exec echo rm -rf {} +   # Sanity check
    find * -prune -mtime +1 -ls -exec rm -rf {} +
    ```

8. Next I checked the 300 hosts that were no longer in inventory with
  a quick ping test from the Superhub:

    ```
    for h in $(cat gone_from_inventory.txt ); do echo "$h" && ping -c 1 -W 1 "$h"; done
    ```

114 of these 300 hosts were not in DNS, which is good.  184 of them have
DNS entries but do not respond to ping.  2 of them DO respond to ping;
one of these is in CFEngine under another name now (there are two DNS
entries for [ip redacted]), and the other should be gotten back into
CFEngine inventory.

I've written separate emails to get the 184 DNS entries cleaned up,
and to get the 1 host back into inventory.

Best,
--Mike Weilgart
