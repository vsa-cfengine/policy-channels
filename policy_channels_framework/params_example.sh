VCS_TYPE="GIT_POLICY_CHANNELS"
GIT_URL="git@gitlab.yoururl.net:not-a-real-project/edit-your-params-file.git"
dir_to_hold_mirror="/opt/cfengine"
ROOT="/opt/cfengine/masterfiles_staging"
# ROOT is not used for this VCS_TYPE but must be present
# for integration with Design Center.
#PKEY="/opt/cfengine/userworkdir/admin/.ssh/id_rsa.pvt"
SCRIPT_DIR="/var/cfengine/httpd/htdocs/api/dc-scripts"
export PATH="${PATH}:/var/cfengine/bin"
#export PKEY="${PKEY}"
export GIT_SSH="${SCRIPT_DIR}/ssh-wrapper.sh"

# Usually "masterfiles" contents e.g. promises.cf is directly in the top level
# of the git repo, so there can be no additional subdirectories.  In environments
# where policy is in a subdir of the git repo (e.g. alongside data files), set
# the variable MASTERFILES_SUBDIR to the name of the subdirectory.
MASTERFILES_SUBDIR=""
# If you set this, you need to also set the CFEngine variable
# def.policy_channels[masterfiles_subdir] to match.
# This can be set in the def.json file like so:
#
#   {
#     vars:
#       "policy_channels" : {
#         "masterfiles_subdir": "subdir_name_here"
#       }
#   }

chan_deploy="/var/cfengine/policy_channels"
# chan_deploy is not used outside this file
# and is just for convenience in defining the channel_config array.

channel_config=()
#channel_config+=( "/var/cfengine/masterfiles" "put-version-tag-here" )
channel_config+=( "$chan_deploy/hub"   "put-version-tag-here" )
channel_config+=( "$chan_deploy/hosts" "put-version-tag-here" )

### Examples:
# channel_config+=( "$chan_deploy/channel_name1" "some_tag" )
# channel_config+=( "$chan_deploy/channel_name2" "commit_hash" )
# channel_config+=( "$chan_deploy/sandbox_channel" "branch_name" )

# Note that channel_config must have an even number of elements
# and that absolute pathnames must be used.
# The format is, after the initial empty array value is set:
# channel_config+=( "/absolute/path/to/deploy/to"  "git_reference_specifier" )
# The git refspec can be a tag, a commit hash, or a branch name
# and will work correctly regardless.
