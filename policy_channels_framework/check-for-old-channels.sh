#!/bin/bash
#
# Author: Mike Weilgart <mike.weilgart@verticalsysadmin.com>
# Date: 22 September 2016
#
# This is a long one-liner that compares the channels defined in 'params.sh'
# to the channel assignment files actually present in
# '/var/cfengine/host_channel_assignments', and reports on any differences
# using 'diff -y'.
#
# It is written as a one-liner so it can be copied and pasted easily
# to your terminal.
#
# Channels that only exist as host assignment files and are not defined
# in params.sh should be defined in params.sh--unless the host being
# assigned no longer exists, in which case the host assignment file should
# be removed.
#
# Channels that only exist in params.sh and not in host assignment files
# should generally be removed--however if there are still copies of host
# assignment files for that channel "in the wild," this can create problems.
# There may be hosts that were assigned to channels by manual editing of
# policy_channel.txt on the host itself, without the assignment file ever
# being on the hub.  Channels assigned to in this manner should not be removed.

diff -y <(echo "DEFINED IN PARAMS"; . /var/cfengine/channel_scripts/params.sh; set -- "${channel_config[@]}"; while [ "$#" -gt 1 ]; do [ "$1" != "/var/cfengine/masterfiles" ] && printf '%s\n' "${1##/var/cfengine/policy_channels/}"; shift 2; done | sort -u) <(echo "IN USE BY HOST ASSIGNMENTS"; sed -nep /var/cfengine/host_channel_assignments/*.txt | sort -u)
